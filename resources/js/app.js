/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { Form, HasError, AlertError } from 'vform'
window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)


import VueRouter from 'vue-router'
Vue.use(VueRouter)
let routes = [
  { path: '/admin', component: require('./components/admin/Index.vue').default },
  { path: '/admin/committee/:id', component: require('./components/admin/Committee').default },
  { path: '/applicant/committee/:id', component: require('./components/applicant/Committee').default },
]
const router = new VueRouter({
    mode: 'history',
    routes,
})

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('dashboard', require('./components/admin/Index.vue').default);
Vue.component('committee', require('./components/admin/Committee').default);
Vue.component('applicant', require('./components/applicant/Index').default);
Vue.component('applicant-committee', require('./components/applicant/Committee').default);
Vue.component('user-committee', require('./components/committee/Index').default);
Vue.component('chat-messages', require('./components/chat/ChatMessage').default);
Vue.component('chat-form', require('./components/chat/ChatForm').default);


const app = new Vue({
    el: '#app',
    router,
    data: {
      messages: []
    },

    created() {
        this.fetchMessages();
        Echo.private('chat')
        .listen('MessageSent', (e) => {
          this.messages.push({
            message: e.message.message,
            user: e.user
          });
        });
    },

    methods: {
        fetchMessages() {
            axios.get('/messages').then(response => {
                this.messages = response.data;
            });
        },

        addMessage(message) {
            this.messages.push(message);

            axios.post('/messages', message).then(response => {
              console.log(response.data);
            });
        }
    }
});
