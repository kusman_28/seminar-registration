<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    public function users()
    {
        return User::latest()->get();
    }

    public function approve(Request $request)
    {
        $approve = User::findOrFail($request->id);
        $approve->approved_at = now();
        $approve->save();
    }
}
