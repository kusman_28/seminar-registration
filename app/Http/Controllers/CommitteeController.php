<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;

class CommitteeController extends Controller
{
    function profile() {
        return auth('web')->user();
    }

    public function updateProfile(Request $request)
    {
        $user = auth('web')->user();

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|max:191|unique:users,email,
            '.$user->id,
            'password' => 'sometimes|required|string|min:8'
        ]);

        $currentPhoto = $user->photo;
        if ($request->photo != $currentPhoto) {
            $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

            \Image::make($request->photo)->save(public_path('image/profile/').$name);

            $request->merge(['photo' => $name]);


            $userPhoto = public_path('image/profile/').$currentPhoto;
            if (file_exists($userPhoto)) {
                @unlink($userPhoto);
            }
            
        }

        $currentBanner = $user->banner;
        if ($request->banner != $currentBanner) {
            $name = time().'.' . explode('/', explode(':', substr($request->banner, 0, strpos($request->banner, ';')))[1])[1];

            \Image::make($request->banner)->save(public_path('image/banner/').$name);

            $request->merge(['banner' => $name]);


            $userBanner = public_path('image/banner/').$currentBanner;
            if (file_exists($userBanner)) {
                @unlink($userBanner);
            }
            
        }

        if (!empty($request->password)) {
            $request->merge(['password' => Hash::make($request['password'])]);
        }

        $user->update($request->all());
        // return ['message' => "Success"];
    }
}
