<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.index');
    }

    public function applicant()
    {
        return view('applicant.index');
    }

    public function committee()
    {
        return view('committee.index');
    }

    public function showCommittee($id)
    {
        return User::findOrFail($id); 
    }
    public function showApplicantCommittee($id)
    {
        return User::findOrFail($id); 
    }
}
