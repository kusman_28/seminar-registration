<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Applicant User',
            'email' => 'applicant@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password123'),
            'role' => 2,
            'approved_at' => null,
        ]);

        \App\User::create([
            'name' => 'Committee User',
            'email' => 'committee@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password123'),
            'role' => 3,
            'approved_at' => null,
        ]);
    }
}
