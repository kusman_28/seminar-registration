<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/approval', function () {
    return view('approval');
});

Auth::routes();

Route::post('/login', [
    'uses'          => 'Auth\LoginController@login',
    'middleware'    => 'checkstatus',
]);
Route::post('/register', [
    'uses'          => 'Auth\RegisterController@register',
    'middleware'    => 'checkstatus',
]);

// Admin Routes
Route::get('/admin', 'HomeController@index')->name('home');
Route::get('/admin/committee/{id}', 'HomeController@showCommittee');
Route::get('/users', 'AdminController@users');
Route::put('/user/{id}', 'AdminController@approve');

// Applicant Routes
Route::get('/applicant', 'HomeController@applicant');
Route::get('/applicant/committee/{id}', 'HomeController@showApplicantCommittee');

// Committee Routes
Route::get('/committee', 'HomeController@committee');
Route::get('profile', 'CommitteeController@profile');
// Route::put('profile', 'CommitteeController@update');
Route::put('profile', 'CommitteeController@updateProfile');

// Chat Routes
Route::get('/chat', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');